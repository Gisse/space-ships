Space Ships

Purpose of document: Explaining the basic idea behind Space Ships game.

Basic idea: 
Space Ships is a game in which players assemble ships and then match them up against other players' ships in combat. By participating and winning battles, player advance levels and gain improvements.

Leveling up:

User will level up with points he earn in battles. With higher level user will have access to more parts for space ships. 

In game money:
Ingame money can be earned with winning battles, coupons, achievements or bought with real money. Players can spend them on:
- buying parts for ships.
- ingame money can also win more “hourly battles” vs AI. The price of another battle slot should roughly equal to that of winning a battle. The player gets experience whether he wins or loses, but money only comes from winning. So, effectively, players can exchange money for getting more and more experience. Which is good for us too :)
- in game money buys expendables, like missile refills.
- completely new ships :)


Experience
- By winning and losing battles gets experience( more/less, varies on levels). Experience unlocks levels.
- Levels unlock additional slots, like rear lasers.
- level groups can also unlock bigger ships or diff. classes.
-Clasic rock, paper scissors logic for everything


IMPORTANT !!!!

Game need to be simple. Game description (no tech details) should always and forever fit onto this first page.




User stories (stuff to develop)

Story 1. First battle.
Upon landing on the home page, first-time visitor (cookie detection) will see a ship blueprint in the middle and a selection of parts to choose from.
Parts for an initial ship:
- 1 basic engine (nuclear engine)
- 1 of two choices of forward lasers (one can do burst fire, but with lower aim, the other is more powerful and precise, but slow)
- 1 basic shield to equip
- the missile slot is empty.
(more weapon and gear slots will come in later in the game)
Player is invited to assemble the 3 basic parts and Engage in a combat.
This first-ever combat will be against an AI-controlled ship, and it will have 99% chance for the player to win.
- after the victory, user is invited to register for an account and make further upgrades.

Note: returning users will have a cookie by which we’ll identify them as returning. They will also see the first initial screen but they’ll be able to click on “Log in” instead.
If they have a session (remember me) associated, they’ll be taken to the user dashboard right away.

Story 2. Registration
User is presented with “Register with Facebook/Register with Google/Register with Twitter” or just an email box. (should they so choose, they’ll be able to enter more details later to enter more details, like full name, credit card number XD etc.)
Beneath the “Register with…” buttons, there’s a friendly sentence: “we won’t post on your behalf, unless you allow us to.”
The user also needs to tick a checkbox “I agree with terms of service.”
There’s a button “Create my account.”
Upon clicking Create my account button, the following happens:
- if the user registered with external services, he’ll get an OAuth2 popup to allow our app to use the service.
- if the user registered with email, he’ll get a message: “We sent you a confirmation email. Continue to play the game, and remember to confirm your email address within 24 hours.”
(note: if he later logs in with unconfirmed account, he’ll not be able to play until clicking confirm link)
- the user is finally taken to his dashboard.

Story 3. Picking a combat vs. AI
We need to account for the first stages when there aren’t many players to choose from, we need to prepare AI ships as enemies. Player can play 3 AI battles per hour to gain experience (not as much as vs human players). (Paid account can play 4 battles/hour).
- there’s a button on the dashboard: “Play vs AI” with “xx battles left this hour”
- clicking it, the user gets a dialog to select from:
“Perfect match”.
“Play against weaker ships for easy fast money’
“Play against stronger ships for greater reward”
Note: perfect match vs AI should make the player win around 65-70% of the time. Weaker ships battles are won 90% of the time, with exp and money cut by 30%. Stronger ships should be hard to win, cca 25% chance, but with 50-60% more rewards
Picking a match size, player proceeds to the combat screen.

Story 4. Combat screen
Sections:
- ship movement
- damage
- energy
- structure points
- targeting

After clicking on dialog with selected type of battle player will see two ships which fire on each other.  After screen is gone we are showing results of battle to user and writing with big text WIN or LOSE. In battle report is placed in details what part of ship got hit with what weapon so the user know what make him lose or win.

-ship movement
 and that will represent all the movement player can do?????
- stance and energy
User will be able to change stance of his ship during battle with adjusting it from aggressive to defensive. Aggressive tends to give more energy to weapons, defensive to shields. Energy of a ship should be sparse and not enough to always fully power everything, so weapons with a fully aggressive stance work with 100% power, shields with 50% power, and the other way around.
Default stance for each battle is set from user preferences, which is in the middle - 75% power to weapons, 75%power to shields.

-damage
Hit points of ship are shown acumulatively for all parts of ship in bar structure type. Destruction of specific components like shields will bring to faster lose of hit points of ship.

- shield
internal mechanic, not shown to player, is that the fully powered shield increases evade chance (so the opponent misses more) AND decreases damage dealt. So, damaged shield will only function with less power.

to add engine power for maneuvers? good question :) Maybe keep it simpler for now and see what we come up with?ok


-structure points
Hit points of ship are dispersed all over ship parts. In case that main reactor is destroyed it is not important how many hit points user is left as with that he lose main power source and ability to fight further.
 - weapon is  > 10% of all structure points  
 - shields are <> 25% of all structure points
 - engines are <> 20% of all structure points
 - main reactor <> 10% of all structure points
 - hull <> what have left

-targeting
While player is on lower levels there won’t be any targeting options for him. With leveling up he will get ability to buy targeting computer and to instal it. With that option he will obtain ability to target some of its weapons to specific subsystems of enemy before battle starts.

- tick (backend sync stuff) (technical stuff here, do not read if you no engi)
The battle progresses over time. The battle is not real time, but is split into “ticks” - 3 second intervals. Each interval, new ship positions, options, targets etc are sent to the server, which calculates and returns the results. This ensures that the client takes the burden of handling all the on-screen action, but the server does actual damage calculations. So, no cheats.


Ranking list

Ranking list is part of game. Players will have ability to check they rank among their friends and among whole world. Players will be ranked by score, level, amount earned points and maximum damage dealt in one game.

Data structures
User
id - mandatory, unique
email - unique
- fb ID and fb public data
- google id and google public data
- ships: array
- credits: long
- realMoney: long

Ship
- class
- slots: array of items

Item
(weapon, shield, engine, special)

Weapon
- type: laser, missile, piercing
- modification: burst, high-precision, high-power, shield-piercing (bypasses shield)
- slot: forward-facing, extended-forward, rear-facing, side, 360

Battle
- timestamp
- who won
- who played
- how many exp they got




Game Engines

http://unity3d.com
http://www.projectanarchy.com/
http://www.unrealengine.com/udk/
https://www.scirra.com/


Graphical design

Here is some examples of graphical design for space ships
http://www.over00.com/?p=1844
http://comps.gograph.com/cartoon-spaceship-icon-set_gg58250644.jpg
http://comps.gograph.com/spacecraft-spaceship-vector-set_gg60454563.jpg
http://behance.vo.llnwd.net/profiles22/1386759/projects/7252301/3a0008604728f540232248fe1c9a8224.jpg
http://dirkloechel.deviantart.com/art/Size-Comparison-Science-Fiction-spaceships-398790051

Here are space ship parts!!!!
http://opengameart.org/content/space-ship-construction-kit


